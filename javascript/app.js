const grid = document.querySelector(".grid-container");
const clear = document.querySelector(".clear");
const fader = document.querySelector("#fader");
const squaresNumber = document.querySelector("#squares-number");
const cellColor = document.querySelector("#cell-color");
const gridColor = document.querySelector("#grid-color");
const GRID_WIDTH = 600;
const GRID_HEIGHT = 600;

let gridSize = 0;

function updateGrid(event) {
  const size = fader.value;
  gridSize = size;
  squaresNumber.value = `${size} × ${size}`;
  drawGrid();
}

function drawGrid() {
  const rows = [];
  grid.style.backgroundColor = gridColor.value;
  for (let row = 0; row < gridSize; ++row) {
    const gridRow = document.createElement("div");
    gridRow.classList.add("grid-row");
    gridRow.style.width = `${GRID_WIDTH}px`;
    gridRow.style.height = `${GRID_HEIGHT / gridSize}px`;
    for (let col = 0; col < gridSize; ++col) {
      const gridCell = document.createElement("div");
      gridCell.classList.add("grid-cell");
      gridCell.style.width = `${GRID_WIDTH / gridSize}px`;
      gridCell.style.height = `${GRID_HEIGHT / gridSize}px`;
      gridRow.appendChild(gridCell);
    }
    rows.push(gridRow);
  }
  grid.replaceChildren(...rows);
}

function updateGridBackground(event) {
  grid.style.backgroundColor = event.target.value;
}

function updateCellsColors(event) {
  const paintedCells = document.querySelectorAll(".cell-painted");
  paintedCells.forEach(function (cell) {
    cell.style.backgroundColor = event.target.value;
  });
}

function drawCell(event) {
  if (
    event.target instanceof HTMLElement &&
    event.target.classList.contains("grid-cell")
  ) {
    event.target.style.backgroundColor = cellColor.value;
    event.target.classList.add("cell-painted");
  }
}

function init() {
  grid.addEventListener("mouseover", drawCell);
  fader.addEventListener("change", updateGrid);
  clear.addEventListener("click", drawGrid);
  gridColor.addEventListener("change", updateGridBackground);
  cellColor.addEventListener("change", updateCellsColors);
  updateGrid();
}

init();
